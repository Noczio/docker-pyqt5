# Docker image for Pyqt5

This repo is an example of how to modify the original image from jozo/pyqt5 which runs on ubuntu 20.04

1. It makes it possible to use a virtual venv and install dependencies with pip3 on that enviorment
2. The virtual enviortment can be installed anywhere, for this repo will be in the opt directory with the name of pyqt5venv

# How to test

Build image from dockerfile
> docker build -t pyqt5:test .

Test that it opens the main.py on host display
> docker run --rm -it -e DISPLAY=$DISPLAY -u qtuser -v /tmp/.X11-unix:/tmp/.X11-unix pyqt5:test

To open this on MacOS please do as jozo says in https://hub.docker.com/r/jozo/pyqt5