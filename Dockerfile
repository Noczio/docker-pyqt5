FROM jozo/pyqt5:1.0

# Install required packages
RUN apt-get update && \
    apt-get install -y \
    python3-pip && \
    rm -rf /var/lib/apt/lists/*

RUN pip3 install setuptools virtualenv

ENV WHICH_PYTHON3=/usr/bin/python3
ENV WHICH_VIRTUALENV=/usr/bin/virtualenv
ENV VIRTUAL_ENV=/opt/pyqt5venv

RUN virtualenv -p $WHICH_PYTHON3 $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip3 install pyqt5

# Set the working directory
WORKDIR /app

# Copy your application code into the container
COPY main.py /app

# Set the entry point to launch the application
ENTRYPOINT ["python3", "main.py"]