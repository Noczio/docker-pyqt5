import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QPushButton

class MyApp(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        self.setWindowTitle('MyApp')

        self.label = QLabel(self)
        self.label.setText('Hello, world!')
        self.label.move(50, 50)

        self.button = QPushButton('Click me!', self)
        self.button.move(50, 80)
        self.button.clicked.connect(self.on_button_click)

        self.setGeometry(100, 100, 200, 150)
        self.show()

    def on_button_click(self):
        self.label.setText('Button clicked!')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyApp()
    sys.exit(app.exec_())